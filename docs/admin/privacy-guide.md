# Privacy guide

## GDPR

PeerTube core (without installed plugins) is GDPR compatible:

 * It doesn't send personal data to any third party if P2P is disabled
 * Users can delete their account and all their data will be deleted
 * Users can export their data and import it into another instance (in PeerTube >= 6.1)
 * Admins can fill in the *Terms* section of the PeerTube instance that users must accept to register

Below are the details details of personal data processed by PeerTube without plugins installed.

### PeerTube logs

PeerTube records in its logs for each HTTP request. This can be disabled in [PeerTube configuration](/maintain/configuration#peertube-configuration).

**Persons concerned**
 * Any visitor

**Purpose**:
 * Legal obligation

**Processed data**:
 * Client IP (can be anonymized in [PeerTube configuration](/maintain/configuration#peertube-configuration))
 * User agent
 * URL of the visited page
 * Date and time of visit

**Data source**
 * User web browser/web client

**Data retention**:
 * Depends on the server configuration

**Recipients**
 * System administrators
 * PeerTube administrators


### PeerTube database

No personal information is stored in the database for anonymous users. PeerTube only stores data from registered users.

**Persons concerned**
 * Registered users

**Purpose**:
 * Manage user account (login, password recovery): legitimate interest
 * Automatically resume videos/retrieve previously viewed videos: legitimate interest
 * Manage user videos (upload/watch): legitimate interest

**Collected data**:
 * User Email, Username, Password
 * Video history (can be disabled by the user)
 * Published videos

**Data source**
 * Information provided by the user

**Data retention**:
 * As long as the account is not deleted

**Recipients**
 * System administrators
 * PeerTube administrators

### Player P2P

PeerTube uses the [BitTorrent](https://en.wikipedia.org/wiki/BitTorrent)/[WebTorrent](https://en.wikipedia.org/wiki/WebTorrent) protocols to share bandwidth between users to help lower the load on the server.

This feature **can be disabled** by the PeerTube admin or directly in users settings.

**Persons concerned**
 * Any visitor that is watching a video

**Purpose**:
 * Reduce server load: legitimate interest
 * Improve playback quality: legitimate interest

**Processed data**:
 * IP address

**Data source**
 * User web browser/web client

**Data retention**:
 * As long as the user is watching the video

**Recipients**
 * System administrators
 * Other peers that are watching the video, and so potentially with remote peers **outside the European Union**

## Data stored in web browser

### Cookies

Here is a list of cookies used by PeerTube:

 `clientLanguage`: remember the user's manually selected page language (3 months)

### Local storage

Here is a list of data stored in web browser local storage (without time limit):

Anonymous & logged-in users:

 * `app-broadcast-message-dismissed`: remember the user closed the message broadcasted by the admin
 * `video-watch-privacy-concern`: remember the user closed the P2P related privacy message in video watch page
 * `client_id` & `client_secret`: OAuth2 tokens generated globally for the instance used to do HTTP requests to the PeerTube REST API.
 These tokens are the same for all users (logged-in/anonymous) on the instance
 * `peertube-hotkeys-disabled`: remember the used disabled keyboard shortcuts
 * `nsfw_policy`: remember user preferences about NSFW videos to filter videos
 * `video_languages`: remember video languages selected by the user to filter videos
 * `theme` & `last_active_theme`: remember the interface theme selected by the user
 * `peertube-videojs-*` & `auto_play_video` & `auto_play_next_video` & `auto_play_video_playlist`: remember user preferences about the player
 * `peertube-videojs-video-watch-history`: remember videos with their last watched time code to automatically resume videos

Logged-in users only:

 * `access_token` & `refresh_token` & `token_type`: OAuth2 tokens generated for the user after a successful login and used to do HTTP requests to the PeerTube REST API
 * `id`, `username`, `email`, `role`: user attributes after successful login to retrieve user state after returning to the website
 * `video-redundancies-list-display-type`: remember user redundancy display type in admin
 * `admin-user-list-selected-columns`: remember selected columns by the admin
 * `jobs-list-state` & `jobs-list-type`: remember jobs list table state/type
 * `admin-logs-log-type-choice`: remember logs type choice
 * `rest-table-sort-*`: remember tables column sort
 * `no_account_setup_warning_modal`: remember the user don't want to see the welcome modal again
 * `no_welcome_modal`: remember the admin don't want to see the welcome modal again
 * `no_instance_config_warning_modal`: remember the admin don't want to see the instance warning modal again

### Session storage

Here is a list of data stored in web browser session storage (deleted when the page session ends):

Anonymous & logged-in users:

 * `redirect-latest-session-url`: track the last page visited to redirect the user on the previous page in some situation (login for example)
 * `peertube-videojs-session-id`: track the web browser session by randomly generating an identifier that is sent to the server to identify the user as a unique viewer. The server doesn't keep this session ID in its database and only uses it to anonymously track the viewer's progress through a particular video.

## PeerTube P2P & privacy

To reduce server load and improve playback quality, PeerTube uses the [BitTorrent](https://en.wikipedia.org/wiki/BitTorrent)/[WebTorrent](https://en.wikipedia.org/wiki/WebTorrent) protocols to share bandwidth between users that are watching the same video.

**P2P can be disabled by the PeerTube admin or directly in users settings.**

If enabled, the main threat to user privacy is that the IP address is stored in the instance's BitTorrent/WebTorrent tracker as long as the user is downloading or watching the video.

**What are the consequences?**

In theory, someone with enough technical skills could create a script that tracks which IP is downloads which video. In practice, this is much more difficult because:

 * An HTTP request must be sent to each tracker for each video to spy. To spy all the videos on PeerTube, we need to send as many requests as there are videos (so potentially a lot)
 * For each request sent, the tracker returns a limited number of random peers. For example, if there are 1000 peers in the swarm and the tracker only sends 20 peers for each request, there must be at least 50 requests sent to know every peer in the swarm
 * These requests need to be sent regularly to know who is starting/stopping a video. It is easy to detect that kind of behaviour
 * If an IP address is stored in the tracker, it doesn't mean that the person behind the IP (if they exist) has watched the video
 * The IP address is a vague information: it usually changes regularly and can represent many persons or entities
 * Web peers are not publicly accessible: because we use the websocket transport, the protocol is different from classic BitTorrent trackers. Peers in a web browser send a signal containing the IP address to the tracker, which randomly selects other peers to forward the information to. More details can be found in [this document](https://github.com/yciabaud/webtorrent/blob/beps/bep_webrtc.rst)

The worst-case scenario of an average person spying on their friends is quite unlikely. There are much more effective ways to get this kind of information.

**How does PeerTube compare with YouTube?**

YouTube's privacy threats are different from PeerTube's. In the case of YouTube, the platform collects a huge amount of your personal information (not just your IP) to analyze and track you. In addition, YouTube is owned by Google/Alphabet, a company that tracks you across many websites (via AdSense or Google Analytics).

**What can I do to limit the exposure of my IP address?**

Your IP address is public so every time you visit a website, there are a number of actors (in addition to the final website) that see your IP in their connection logs: ISP/routers/trackers/CDN and more. PeerTube is transparent about this: it warns you that if you want to keep your IP private, you must use a VPN or Tor Browser. It doesn't make sense to think that removing P2P from PeerTube will give you back your anonymity.

**What is PeerTube doing to mitigate this privacy issue?**

PeerTube wants to provide the best possible countermeasures:

 * PeerTube sets a limit to the number of peers sent by the tracker
 * PeerTube sets a limit on the request frequency received by the tracker
 * PeerTube warns admins when it detects suspicious requests
 * PeerTube admins can disable P2P from the administration interface
 * Anonymous and registered users can disable P2P in their settings
