# Setup your account

## Create an account on PeerTube

To be able to upload a video, you must have an account on an _instance_ (a server running PeerTube). The PeerTube project maintains a list of
public instances at [instances.joinpeertube.org](https://instances.joinpeertube.org/instances) (note that instance administrators must add themselves
to this list manually, it is not automatically generated).

Once you've found an instance that suits you, click the button "Create an account" and fill in your **login** (nickname), an **email address** and a **password**.

![Registration page after clicking on "Create an account"](/assets/profile-registration-01.png)

Once this first step done, you have to setup your default channel by indicate its name (`id`) and display's name.

![Channel first creation](/assets/profile-registration-02.png)

## Connect to your PeerTube instance

To log in, you must go to the address of the particular instance that you registered on. Instances share each other's videos, but
each instance's index of accounts is not federated. Click on the "Login" button in the top-left corner, then fill in your login/email address and
password. Note that the login name and password are **case-sensitive**!

Once logged in, your name and display name will appear below the instance name.

## Update your profile

To update your user profile, change your avatar, change your password, etc... Click on <vue-feather type="user"></vue-feather> **My Account** under your profile name in the sidebar. You then have a few options:

* Settings
* Notifications
* Applications
* Moderation
  * Muted accounts
  * Muted servers
  * Abuse reports

![user profile view](/assets/profile-library.png)

### Settings

**Settings** tab has several sections.

Before any of the other sections, you can:

* See/change your Avatar. The allowed filetypes and file-size should be displayed by clicking <vue-feather type="edit-2"></vue-feather> and move the mouse over the upload button.
* View your Upload Quota and used space. Your upload quota is determined by the instance hosting your account. If your instance creates multiple versions of your videos in different qualities then all versions count towards your quota.

#### Profile

* View your Upload Quota and used space. Your upload quota is determined by the instance that hosts your account. If your instance creates multiple versions of your videos in different qualities then all versions count towards your quota.
* Modify your **Display Name**. Note that your Display Name is not the same as your Username (used for logging in), which cannot be modified.
* Add or modify your **User Description**. This will be displayed on your Public Profile in the "About" section.

#### Video Settings

* Change how videos with sensitive content are displayed. You can choose to list them normally, blur their content, or hide them completely. This feature may be disabled on your instance.
* Select which languages you want to see videos in. Languages which are not selected will be hidden. You can have up to 20 languages (or all languages) selected.
* Select if you want to help sharing videos being played or not (by using, or not, P2P).
* Change whether videos autoplay or not.
* Change whether you want to automatically start next video or not.

#### Notifications

Change which notifications you receive. You can choose to receive notifications in PeerTube, via Email, both, or neither for:

  * New video from your subscriptions
  * New comment on your video
  * [admin] New abuse
  * [admin] Video blocked automatically waiting review
  * One of your video is blocked/unblocked
  * Video published (after transcoding/scheduled update)
  * Video import finished
  * [admin] A new user registered on your instance
  * You or your channel(s) has a new follower
  * Someone mentioned you in video comments
  * [admin] Your instance has a new follower
  * [admin] Your instance automatically followed another instance
  * An abuse report received a new message
  * One of your abuse reports has been accepted or rejected by moderators

When **Web** notifications are enabled, there are visible above your avatar:

![notification numbre image](/assets/use-settings-notifications-indicator.png)

By clicking it you can see your notification list. You can click each notification, or:

  * <vue-feather type="inbox"></vue-feather> mark all as read
  * <vue-feather type="settings"></vue-feather> update your notifications preferences

#### Interface

Change your Theme. In earlier versions (<=v1.3.1) the theme was selected in the sidebar.

::: info
You have to refresh the web page to see changes
:::

#### Password

Change your Password. To change your password you will need to enter your current password, and then enter your new password twice before clicking **_Change Password_**.

#### Email

Change your Email. You will need to enter your password again to change this setting.

#### Danger Zone

 Delete your account.

::: danger
**This is irreversible. This will delete all your data, including channels, videos and comments. Content cached by other servers and other third-parties might make longer to be deleted.**

You need to confirm your username to proceed. If you delete your account, you won't be able to create another with the same username on the instance!
:::

### Account import / export

<Badge type="info" text="PeerTube >= 6.1"></Badge>

::: info
Since 6.1 version of PeerTube you can export an account form an instance to another one. However, this is not a migration (yet)!
:::

#### Account export

<Badge type="info" text="PeerTube >= 6.1"></Badge>

You can request an archive of your account containing:

 * Your account settings with avatar file
 * Your channels with banner and avatar files
 * Your muted accounts and servers
 * Your comments
 * Your likes and dislikes
 * Your subscriptions and followers
 * Your video playlists with thumbnail files
 * Your videos with thumbnail, caption files. Video files can also be included in the archive
 * Your video history

The exported data will contain multiple directories:

 * A directory containing an export in ActivityPub format, readable by any compliant software
 * A directory containing an export in custom PeerTube JSON format that can be used to re-import your account on another PeerTube instance
 * A directory containing static files (thumbnails, avatars, video files etc.)

You can only request one archive at a time.

An email will be sent when the export archive is available.

To **export** your account, you have to click:

  1. **My account** in left menu
  1. **Import/Export** tab
  1. scroll down to **EXPORT** section and click the **Request a new archive** button.

![export an account view image](/assets/export_account.png)

Once done, you will have to click **Include video files in archive file** (including video files is required if you want to re-import your videos on another PeerTube website) and click the **Request an archive** button.

![image popup archive request](/assets/export_archive_popup.png)

You will be notyfied by email and will see your archive by refreshing the page:

![image list of archives](/assets/list_export_archives.png)

#### Account import

<Badge type="info" text="PeerTube >= 6.1"></Badge>

You can import an archive created by another PeerTube website.

This is an import tool and not a migration tool. It's the reason why data (like channels or videos) is duplicated and not moved from your previous PeerTube website.

The import process will automatically:

 * Update your account metadata (display name, description, avatar...)
 * Update your user settings (autoplay or P2P policy, notification settings...). It does not update your user email, username or password
 * Add accounts/servers in your mute list
 * Add likes/dislikes
 * Send a follow request to your subscriptions
 * Create channels if they do not already exist
 * Create playlists if they do not already exist
 * Add watched videos in your video history
 * If the archive contains video files, create videos if they do not already exist

The following data objects are not imported:

 * Comments
 * Followers (accounts will need to re-follow your channels)

An email will be sent when the import process is complete.

To **import** your account, you have to click:

  1. **My account** in left menu
  1. **Import/Export** tab
  1. scroll down to **IMPORT** section and click the **Select the archive file to import** button.

![import an account view image](/assets/import_account.png)
