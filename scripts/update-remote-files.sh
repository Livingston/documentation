#!/bin/bash

set -eu

rm -rf ./docs/remote-markdown ./docs/remote-code

for i in CREDITS.md CHANGELOG.md CODE_OF_CONDUCT.md; do
  echo "Downloading $i"
  curl --no-progress-meter --fail https://framagit.org/framasoft/peertube/PeerTube/-/raw/develop/$i --create-dirs -o ./docs/remote-markdown/$i
done

echo "Downloading contributing guide"
curl --no-progress-meter --fail https://framagit.org/framasoft/peertube/PeerTube/-/raw/develop/.github/CONTRIBUTING.md --create-dirs -o ./docs/remote-markdown/CONTRIBUTING.md

for i in dependencies.md \
  production.md \
  translation.md \
  docker.md \
  tools.md \
  plugins/guide.md \
  api/quickstart.md \
  api/embeds.md \
  development/ci.md \
  development/lib.md \
  development/localization.md \
  development/monitoring.md \
  development/release.md \
  development/server.md \
  development/tests.md;
do
  echo "Downloading $i file"
  curl --no-progress-meter --fail "https://framagit.org/framasoft/peertube/PeerTube/-/raw/develop/support/doc/$i" --create-dirs -o "./docs/remote-markdown/doc/$i"
done

for i in packages/models/src/plugins/server/server-hook.model.ts \
  client/src/types/client-script.model.ts \
  client/src/types/register-client-option.model.ts \
  server/core/types/plugins/plugin-library.model.ts \
  server/core/types/plugins/register-server-auth.model.ts \
  server/core/types/plugins/register-server-option.model.ts \
  packages/models/src/activitypub/objects/cache-file-object.ts \
  packages/models/src/activitypub/objects/playlist-element-object.ts \
  packages/models/src/activitypub/objects/playlist-object.ts \
  packages/models/src/activitypub/objects/video-comment-object.ts \
  packages/models/src/activitypub/objects/video-object.ts \
  packages/models/src/activitypub/activitypub-actor.ts \
  packages/models/src/plugins/client/client-hook.model.ts \
  packages/models/src/plugins/client/plugin-client-scope.type.ts \
  packages/models/src/plugins/client/plugin-element-placeholder.type.ts \
  packages/models/src/plugins/client/plugin-selector-id.type.ts \
  packages/models/src/plugins/client/register-client-form-field.model.ts \
  packages/models/src/plugins/client/register-client-hook.model.ts \
  packages/models/src/plugins/client/register-client-settings-script.model.ts \
  packages/models/src/plugins/server/managers/plugin-playlist-privacy-manager.model.ts \
  packages/models/src/plugins/server/managers/plugin-settings-manager.model.ts \
  packages/models/src/plugins/server/managers/plugin-storage-manager.model.ts \
  packages/models/src/plugins/server/managers/plugin-transcoding-manager.model.ts \
  packages/models/src/plugins/server/managers/plugin-video-category-manager.model.ts \
  packages/models/src/plugins/server/managers/plugin-video-language-manager.model.ts \
  packages/models/src/plugins/server/managers/plugin-video-licence-manager.model.ts \
  packages/models/src/plugins/server/managers/plugin-video-privacy-manager.model.ts \
  packages/models/src/plugins/server/register-server-hook.model.ts \
  packages/models/src/plugins/server/settings/register-server-setting.model.ts \
  packages/models/src/activitypub/objects/watch-action-object.ts \
  packages/models/src/videos/transcoding/video-transcoding.model.ts;
do
  echo "Downloading $i file"
  curl --no-progress-meter --fail "https://framagit.org/framasoft/peertube/PeerTube/-/raw/develop/$i" --create-dirs -o "./docs/remote-code/$i"
done
